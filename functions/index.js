/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


const functions = require('firebase-functions');
const admin = require("firebase-admin")
const nodemailer = require('nodemailer');
admin.initializeApp()

const adminEmail = 'mail@locumprofessionalsgh.com';

//google account credentials used to send email
var transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'admin@locumprofessionalsgh.com',
        pass: 'businesswithch@rles1'
    }
});

//Notify pro of account status
exports.notifyProStatus = functions.firestore
    .document('users/{userId}')
    .onUpdate((change, context) => {
		const newValue = change.after.data();
		// ...or the previous value before this update
		const previousValue = change.before.data();
		
		var emailSubject = '';
		var emailContent = '';
		var emailTo = newValue.email;
		
		if(newValue.status == 'Active' && previousValue.status == 'Inactive'){
			emailSubject = 'Account Activated';
			emailContent = '<h1>Your Pro Locum Account Activated</h1>'
                            +    '<p>Hi '+ newValue.name + ', your Pro Locum account has been activated. Click this link https://locumprofessionalsgh.com/pro-requested-appt to access your Locum dashboard. </p>';
			sendEmailNotification(emailTo, emailSubject, emailContent);				
							
		}else if(newValue.status == 'Inactive' && previousValue.status == 'Active'){
			emailSubject = 'Account Deactivated';
			emailContent = '<h1>Your Pro Locum Account Deactivated</h1>'
                            +    '<p>Hi '+ newValue.name + ', your Pro Locum account has been deactivated. Contact Admin for help. </p>';
			sendEmailNotification(emailTo, emailSubject, emailContent);				
		}else if(newValue.name != previousValue.name || newValue.profile_pic_url != previousValue.profile_pic_url){
			
			
			if(newValue.role == 'PRO'){
				var queryTransactions = admin.firestore().collection("transactions").where("to_id", "==", newValue.uid);
				
				queryTransactions.get().then(function(querySnapshot) {
					querySnapshot.forEach(function(doc) {
						var docRef = admin.firestore().collection("transactions").doc(doc.id);
						docRef.update({to_name : newValue.name, to_profile_pic : newValue.profile_pic_url});
						
					});
				});	
					
				var queryReviews = admin.firestore().collection("reviews").where("to_id", "==", newValue.uid);
				
				queryReviews.get().then(function(querySnapshot) {
					querySnapshot.forEach(function(doc) {
						var docRef = admin.firestore().collection("reviews").doc(doc.id);
						docRef.update({to_name : newValue.name, to_profile_pic : newValue.profile_pic_url});
						
					});
				});	
				console.log("User items updated after profile save");
				
			}else{
				
				var queryTransactions = admin.firestore().collection("transactions").where("from_id", "==", newValue.uid);
				
				queryTransactions.get().then(function(querySnapshot) {
					querySnapshot.forEach(function(doc) {
						var docRef = admin.firestore().collection("transactions").doc(doc.id);
						docRef.update({from_name : newValue.name, from_profile_pic : newValue.profile_pic_url});
						
					});
				});	
					
				var queryReviews = admin.firestore().collection("reviews").where("from_id", "==", newValue.uid);
				
				queryReviews.get().then(function(querySnapshot) {
					querySnapshot.forEach(function(doc) {
						var docRef = admin.firestore().collection("reviews").doc(doc.id);
						docRef.update({from_name : newValue.name, from_profile_pic : newValue.profile_pic_url});
						
					});
				});	
				console.log("User items updated after profile save");
			
			}
			
			
		}else{
			console.log("Activity doesn't require notification");
			return;
		}
		
        
    });
	
//Notify users of transaction status	
exports.notifyTransStatus = functions.firestore
    .document('transactions/{transactionId}')
    .onUpdate((change, context) => {
		const newValue = change.after.data();
		// ...or the previous value before this update
		const previousValue = change.before.data();
		
		var emailSubject = '';
		var emailContent = '';
		var emailTo = '';
		
		if(newValue.status == 'Accepted' && previousValue.status == 'Requested'){
			
			var userRef = admin.firestore().collection("users").doc(newValue.from_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					emailSubject = 'Locum Appointment Accepted';
					emailContent = '<h1>Your Locum Appointment has been Accepted</h1>'
									+    '<p>Hi '+ newValue.from_name + ', your Locum appointment with '+ newValue.to_name +' has been accepted. You will receive invoice soon. Use this link https://locumprofessionalsgh.com/client-accepted-appt to make payment  .</p>';
					sendEmailNotification(emailTo, emailSubject, emailContent);
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
					return;
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
							
							
		}else if(newValue.status == 'Paid' && previousValue.status == 'Accepted'){
		
			var userRef = admin.firestore().collection("users").doc(newValue.to_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					emailSubject = 'Locum Appointment Paid';
					emailContent = '<h1>A user has paid an appointment with you.</h1>'
									+    '<p>Hi '+ newValue.to_name + ', a user has paid an appointment with you. Click this link https://locumprofessionalsgh.com/pro-paid-appt to view details and deliver accordingly. </p>';
					sendEmailNotification(emailTo, emailSubject, emailContent);
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
					return;
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});			
							
		}else if(newValue.status == 'Satisfied' && previousValue.status == 'Paid'){
			
			emailTo = adminEmail;
			emailSubject = 'Locum Appointment Completed';
			emailContent = '<h1>Locum Appointment Completed To Client Satisfaction</h1>'
                            +    '<p>Hi Admin, a locum appointment has been completed to client satisfaction. Resume your portal to see details.</p>';
			sendEmailNotification(emailTo, emailSubject, emailContent);				
							
		}else if(newValue.status == 'Dissatisfied' && previousValue.status == 'Paid'){
			
			emailTo = adminEmail;
			emailSubject = 'Locum Appointment Completed';
			emailContent = '<h1>Locum Appointment Completed But Client Dissatisfied.</h1>'
                            +    '<p>Hi Admin, a Locum appointment has been completed but client not satisfied with delivery. Resume your portal to see details</p>';
			sendEmailNotification(emailTo, emailSubject, emailContent);				
							
		}else{
			console.log("Activity doesn't require notification");
			return;
		}
		
	
    });
	
	
//Notify pro of new transactions	
exports.notifyNewTrans = functions.firestore
    .document('transactions/{transactionId}')
    .onCreate((snap, context) => {
		const newValue = snap.data();

		// access a particular field as you would any JS property
		const name = newValue.name;
	  
		var emailSubject = '';
		var emailContent = '';
		var emailTo = '';
		
		var userRef = admin.firestore().collection("users").doc(newValue.to_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					emailSubject = 'Locum Appointment Booking';
					emailContent = '<h1>You have a new booking</h1>'
                            +    '<p>Hi '+ newValue.to_name + ', you have a new appointment with '+ newValue.from_name +'. Click this link https://locumprofessionalsgh.com/pro-requested-appt to see appointment details and accept or decline accordingly.</p>';
					
					sendEmailNotification(emailTo, emailSubject, emailContent);
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
					return;
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
		
		
        
    });
	
	
//Notify pro of new reviews	
exports.notifyNewReview = functions.firestore
    .document('reviews/{reviewId}')
    .onCreate((snap, context) => {
		const newValue = snap.data();

		// access a particular field as you would any JS property
		//const name = newValue.to_name;
	  
		var emailSubject = '';
		var emailContent = '';
		var emailTo = '';
		
		var userRef = admin.firestore().collection("users").doc(newValue.to_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					emailSubject = 'New Locum Service Review';
					emailContent = '<h1>You have a new review</h1>'
                            +    '<p>Hi '+ newValue.to_name + ', you have a new locum service review from '+ newValue.from_name +'. Click this link https://locumprofessionalsgh.com/pro-reviews to see review details.</p>';
					
					sendEmailNotification(emailTo, emailSubject, emailContent);
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
					return;
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
		
		
        
    });	
	
// Email function	
function sendEmailNotification(emailTo, emailSubject, emailContent){
	const mailOptions = {
				from: '"Locum Professionals" <admin@locumprofessionalsgh.com>',
				to: emailTo,
				bcc: adminEmail,
				subject: emailSubject,
				html: emailContent
			};
			return transporter.sendMail(mailOptions, (error, data) => {
				if (error) {
					console.log(error)
					return;
				}
				console.log("Sent!")
			});
}	










//Notify pro of new transactions	
/**
exports.notifyNewTrans = functions.firestore
    .document('transactions/{transactionId}')
    .onCreate((snap, context) => {
		const newValue = snap.data();

		// access a particular field as you would any JS property
		const name = newValue.name;
	  
		var emailSubject = '';
		var emailContent = '';
		var emailTo = '';
		
		if(newValue.to_id == 'Requested'){
			var maillist = [];
			var query = admin.firestore()
								.collection("users")
								.where("status", "==", 'Active').where("services", "array-contains", newValue.service);
				
			query.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					maillist.push(doc.data().email);
				});
			
			emailSubject = 'Locum Appointment Broadcast';
			emailContent = '<h1>New Appointment Broadcast for your service</h1>'
                            +    '<p>Hi, there is a new broadcast demand for your service. Be the first to accept offer. Click this link https://locumprofessionalsgh.com/pro-requested-appt to see appointment details and accept or decline accordingly.</p>';
							
			sendEmailNotification(maillist, emailSubject, emailContent);
			
			}).catch(function(error) {
				console.log("Error getting users offer offers service:", error);
				return;
			});
			
				
			
		}else{
			var userRef = admin.firestore().collection("users").doc(newValue.to_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					emailSubject = 'Locum Appointment Booking';
					emailContent = '<h1>You have a new booking</h1>'
                            +    '<p>Hi '+ newValue.to_name + ', you have a new appointment with '+ newValue.from_name +'. Click this link https://locumprofessionalsgh.com/pro-requested-appt to see appointment details and accept or decline accordingly.</p>';
					
					sendEmailNotification(emailTo, emailSubject, emailContent);
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
					return;
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
			
			
			
		}
		
        
    });
**/

/**	
exports.notifyNewTrans = functions.firestore
    .document('transactions/{transactionId}')
    .onUpdate((change, context) => {
		const newValue = change.after.data();
		// ...or the previous value before this update
		const previousValue = change.before.data();
		
		var emailSubject = '';
		var emailContent = '';
		var emailTo = '';
		
		if(newValue.status == 'Requested' && previousValue.status == 'Accepted'){
			
			var userRef = firebase.firestore().collection("users").doc(newValue.from_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
			
			emailSubject = 'Locum Appointment Accepted';
			emailContent = '<h1>Your Locum Appointment has been Accepted</h1>'
                            +    '<p>Hi '+ newValue.from_name + ', your Locum appointment with '+ newValue.to_name +' has been accepted. Link up with pro to schedule. </p>';
							
							
		}else if(newValue.status == 'Accepted' && previousValue.status == 'Completed'){
			
			var userRef = firebase.firestore().collection("users").doc(newValue.from_id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					emailTo = doc.data().email;
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
				return;
			});
			
			emailSubject = 'Locum Appointment Completed';
			emailContent = '<h1>Your Locum Appointment has been marked Completed</h1>'
                            +    '<p>Hi '+ newValue.from_name + ', your Locum appointment with '+ newValue.to_name +' has been marked completed. Funds will be transfered to '+ newValue.to_name +' accordingly. Kindly contact admin if you are not satisfied with the service(s) rendered by '+ newValue.to_name +'. Thank you.';
		}else{
			return ;
		}
		
        const mailOptions = {
            from: 'Locum',
            to: emailTo,
            subject: emailSubject,
            html: emailContent
        };
        return transporter.sendMail(mailOptions, (error, data) => {
            if (error) {
                console.log(error)
                return
            }
            console.log("Sent!")
        });
    });

**/
	