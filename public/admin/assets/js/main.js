// Signs-in Friendly Chat.
function signIn(email, upassword) {
	firebase.auth().signInWithEmailAndPassword(email, upassword)
	.then(function(){
		window.location.replace("dashboard");	
	})
	.catch(function(error) {
	  // Handle Errors here.
	  var errorCode = error.code;
	  var errorMessage = error.message;
	  //console.log(errorMessage);
	  //console.log(errorCode);
	  document.getElementById('login-resp').innerHTML = 'Invalid credentials';
	  //return errorMessage ;
	  // ...
	});
	
}

function signOut() {
  // Sign out of Firebase.
  firebase.auth().signOut().then(function() {
	  window.location.replace("login");
	  // Sign-out successful.
	}).catch(function(error) {
		console.log(error.message);
	  // An error happened.
	});
}

// Initiate firebase auth.
function initFirebaseAuth() {
  // Listen to auth state changes.
  firebase.auth().onAuthStateChanged(authStateObserver);
}

// Returns the signed-in user's profile Pic URL.
function getProfilePicUrl() {
  return firebase.auth().currentUser.photoURL || '/images/profile_placeholder.png';
}

// Returns the signed-in user's display name.
function getUserName() {
  return firebase.auth().currentUser.displayName;
}

// Returns true if a user is signed-in.
function isUserSignedIn() {
  return !!firebase.auth().currentUser;
}


function changeUserStatus(id, stat){
	Swal.fire({
	  title: 'Change Pro status',
	  text: "Confirm change of pro status to: " + stat,
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  showLoaderOnConfirm: true,
	  allowOutsideClick: () => !Swal.isLoading(),
	  preConfirm: () => {
		return firebase.firestore().collection("users").doc(id).update({
		'status': stat
		})
		.then(function() {
			console.log("Document successfully written!");
			return true;
		})
		.catch(function(error) {
			console.error("Error writing document: ", error);
			return false;
		});
	  
	  
	  }
	  
	}).then((result) => {
	  if (result.value == true) {
		location.reload();
	  }
	});
	
	
	
}





function confirmPayment(id){
	var amount = 0;
	
	Swal.fire({
	  title: 'Confirm Payment',
	  text: "Confirm client payment. Enter the amount(GHS) client paid. You won't be able to revert this action!",
	  input: 'number',
	  inputPlaceholder: '100',
	  inputValidator: (value) => {
		if (!value) {
		  return 'You need to enter amount.'
		}else{
			amount = value;
		}
	  },
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  showLoaderOnConfirm: true,
	  allowOutsideClick: () => !Swal.isLoading(),
	  preConfirm: () => {
		return firebase.firestore().collection("transactions").doc(id).update({
		'status': 'Paid',
		'amount': amount,
		'pay_confirmation_timestamp': firebase.firestore.FieldValue.serverTimestamp()
		})
		.then(function() {
			console.log("Document successfully written!");
			return true;
			
			
		})
		.catch(function(error) {
			console.error("Error writing document: ", error);
			return false;
		});
	  
	  
	  }
	  
	}).then((result) => {
	  if (result.value == true) {
		  //wait(3000);
		location.reload();
	  }
	});

}







function changeTransactionStatus(id, stat){

	Swal.fire({
	  title: 'Change transaction status to: ' + stat,
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  showLoaderOnConfirm: true,
	  allowOutsideClick: () => !Swal.isLoading(),
	  preConfirm: () => {
		return firebase.firestore().collection("transactions").doc(id).update({
		'status': stat
		})
		.then(function() {
			console.log("Document successfully written!");
			return true;
			
			
		})
		.catch(function(error) {
			console.error("Error writing document: ", error);
			return false;
		});
	  
	  
	  }
	  
	}).then((result) => {
	  if (result.value == true) {
		location.reload();
	  }
	});

}



function getSetUserProfile(id){
			var userRef = firebase.firestore().collection("users").doc(id);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					//console.log("User data:", doc.data());
					document.getElementById('user-name').innerHTML = doc.data().name ;
					document.getElementById('user-email').innerHTML = doc.data().email ;
					document.getElementById('user-phone').innerHTML = doc.data().phone ;
					document.getElementById('user-gender').innerHTML = doc.data().gender ;
					document.getElementById('user-profile-pic').src = doc.data().profile_pic_url ;
						
					if(doc.data().role == 'PRO'){
						//User is pro
						var services = doc.data().services || ['No Service'];
						getProTransactions(id);
						getProReviews(id);
						document.getElementById('user-services').innerHTML = services.toString() ;//+ renderRating(parseInt(doc.data().rating_sum / doc.data().rating_count)) + '<span class="d-inline-block average-rating"> ('+ doc.data().rating_count +')</span>';
						document.getElementById('user-about-me').innerHTML = doc.data().about_me || 'No description' ;
						$('#national-id').attr("href", doc.data().national_id_url || '#');
						$('#license').attr("href", doc.data().license_url || '#');
						$('#cv').attr("href", doc.data().cv_url || '#');
					
						
						
					}else{
						//User is Client
						$('#user-pro-details').hide();
						getClientTransactions(id);
						
					}
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
			});
			
			
}



function getClientTransactions(id){
			var query = firebase.firestore()
							.collection("transactions")
							.where("from_id", "==", id);
			
			query.get().then(function(querySnapshot) {
				document.getElementById('transaction-table').innerHTML =
				'<table class="datatable table table-hover table-center mb-0">'
				+	'	<thead>'
				+	'		<tr>'
				+	'			<th>Name</th>'
				+	'			<th>Date</th>'
				+	'			<th>Service</th>'
				+	'			<th>Amount</th>'
				+	'			<th>Status</th>'
				+	'		</tr>'
				+	'	</thead>'
				+	'	<tbody id="transaction-list">'
				+	'	</tbody>'
				+	'</table>';
				
				querySnapshot.forEach(function(doc) {
					//console.log("Transaction history:", doc.data());
					document.getElementById('transaction-list').innerHTML +=
						'<tr>'
						+	'	<td>'
						+	'			<h2 class="table-avatar">'
						+	'			<a href="profile?id='+ doc.data().to_id +'" class="avatar avatar-sm mr-2">'
						+	'			<img class="avatar-img rounded-circle" src="'+ doc.data().to_profile_pic+'" alt="User Image">'
						+	'			</a>'
						+	'			<a href="profile?id='+ doc.data().to_id +'">'+ doc.data().to_name+'</a>'
						+	'			</h2>'
						+	'	</td>'
						+	'	<td>'+ doc.data().date +'</td>'
						+	'	<td>'+ doc.data().service +'</td>'
						+	'	<td>'+ doc.data().amount +'</td>'
						+	'	<td><span class="badge badge-pill bg-success-light">'+ doc.data().status+'</span></td>'
						+	'	</tr>';
				});
			}).catch(function(error) {
				console.log("Error getting transactions history:", error);
			});
		
				
}


function getProTransactions(id){
			var query = firebase.firestore()
							.collection("transactions")
							.where("to_id", "==", id);
			
			query.get().then(function(querySnapshot) {
				
				document.getElementById('transaction-table').innerHTML =
				'<table class="datatable table table-hover table-center mb-0">'
				+	'	<thead>'
				+	'		<tr>'
				+	'			<th>Name</th>'
				+	'			<th>Date</th>'
				+	'			<th>Service</th>'
				+	'			<th>Amount</th>'
				+	'			<th>Status</th>'
				+	'		</tr>'
				+	'	</thead>'
				+	'	<tbody id="transaction-list">'
				+	'	</tbody>'
				+	'</table>';
				
				
				querySnapshot.forEach(function(doc) {
					//console.log("Transaction history:", doc.data());
					document.getElementById('transaction-list').innerHTML +=
						'<tr>'
						+	'	<td>'
						+	'			<h2 class="table-avatar">'
						+	'			<a href="profile?id='+ doc.data().to_id +'" class="avatar avatar-sm mr-2">'
						+	'			<img class="avatar-img rounded-circle" src="'+ doc.data().from_profile_pic+'" alt="User Image">'
						+	'			</a>'
						+	'			<a href="profile?id='+ doc.data().to_id +'">'+ doc.data().from_name+'</a>'
						+	'			</h2>'
						+	'	</td>'
						+	'	<td>'+ doc.data().date +'</td>'
						+	'	<td>'+ doc.data().service +'</td>'
						+	'	<td>'+ doc.data().amount +'</td>'
						+	'	<td><span class="badge badge-pill bg-success-light">'+ doc.data().status+'</span></td>'
						+	'	</tr>';
				});
			}).catch(function(error) {
				console.log("Error getting transactions history:", error);
			});
		
				
}



function getProReviews(id){
			var query = firebase.firestore()
							.collection("reviews")
							.where("to_id", "==", id);
			
			query.get().then(function(querySnapshot) {
				document.getElementById('review-table').innerHTML =
				'<table class="datatable table table-hover table-center mb-0">'
				+	'	<thead>'
				+	'		<tr>'
				+	'			<th>Name</th>'
				+	'			<th>Content</th>'
				+	'		</tr>'
				+	'	</thead>'
				+	'	<tbody id="review-list">'
				+	'	</tbody>'
				+	'</table>';
				
				querySnapshot.forEach(function(doc) {
					//console.log("Reviews history:", doc.data());
					document.getElementById('review-list').innerHTML +=
						'<tr>'
						+	'	<td>'
						+	'			<h2 class="table-avatar">'
						+	'			<a href="profile?id='+ doc.data().to_id +'" class="avatar avatar-sm mr-2">'
						+	'			<img class="avatar-img rounded-circle" src="'+ doc.data().from_profile_pic+'" alt="User Image">'
						+	'			</a>'
						+	'			<a href="profile?id='+ doc.data().to_id +'">'+ doc.data().from_name+'</a>'
						+	'			</h2>'
						+	'	</td>'
						+	'	<td>'+ doc.data().content +'</td>'
						+	'	</tr>';
				});
			}).catch(function(error) {
				console.log("Error getting reviews history:", error);
			});
		
				
}





function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      //paramName = paramName.toLowerCase();
      //if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}


// Triggers when the auth state change for instance when the user signs-in or signs-out.
function authStateObserver(user) {
  if (user && user.email == 'support@locumprofessionalsgh.com') { // User is signed in!
    console.log("User is signed in!"); 
    
  } else { // User is signed out!
    window.location.replace("login");
	
  }
}


// Checks that the Firebase SDK has been correctly setup and configured.
function checkSetup() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
  }
}

// Checks that Firebase has been imported.
checkSetup();


// initialize Firebase
//initFirebaseAuth();

//searchDoctors();

// Remove the warning about timstamps change. 
/**
var firestore = firebase.firestore();
var settings = {timestampsInSnapshots: true};
firestore.settings(settings);
**/