var currentUserRef ;
var commission = 10;
const proPages = ["pro-requested-appt", "pro-accepted-appt", "pro-paid-appt", "pro-profile-settings", "pro-reviews", "pro-invoices"];
const clientPages = ["client-requested-appt", "client-accepted-appt", "client-paid-appt", "client-completed-appt", "client-profile-settings", "client-invoices"];

// Signs-in Friendly Chat.
function clientSignIn() {

	// Initialize the FirebaseUI Widget using Firebase.
	var ui = new firebaseui.auth.AuthUI(firebase.auth());	
	
	var uiConfig = {
	  callbacks: {
		signInSuccessWithAuthResult: function(authResult, redirectUrl) {
		  // User successfully signed in.
		  // Return type determines whether we continue the redirect automatically
		  // or whether we leave that to developer to handle.
		  
		  var user = authResult.user;
		  var userRef = firebase.firestore().collection("users").doc(user.uid);
					userRef.get().then(function(doc) {
						if (doc.exists) {
							window.location.replace("client-requested-appt");
					
						} else {
							console.log("New user. Creat Reference");
							firebase.firestore().collection("users").doc(user.uid).set({
								'name': user.displayName,
								'email': user.email,
								'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
								'profile_pic_url': user.photoURL,
								'uid': user.uid,
								'role': 'CLIENT'
							})
							.then(function() {
								console.log("Document successfully written!");
								window.location.replace("client-requested-appt");
							})
							.catch(function(error) {
								console.error("Error writing document: ", error);
							});
						}
					}).catch(function(error) {
						console.log("Error getting user:", error);
					});
			
			
		  return false;
		},
		uiShown: function() {
		  // The widget is rendered.
		  // Hide the loader.
		  document.getElementById('loader').style.display = 'none';
		}
	  },
	  // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
	  signInFlow: 'popup',
	  signInSuccessUrl: '<url-to-redirect-to-on-success>',
	  signInOptions: [
		// Leave the lines as is for the providers you want to offer your users.
		firebase.auth.GoogleAuthProvider.PROVIDER_ID,
		firebase.auth.FacebookAuthProvider.PROVIDER_ID,
		firebase.auth.EmailAuthProvider.PROVIDER_ID
	  ],
	  // Terms of service url.
	  tosUrl: 'https://docs.google.com/document/d/1MZTxDNvX1Q9DvgdFj3gT_pN0G_bltFqOcz65Q5kW3Ys/edit?usp=sharing',
	  // Privacy policy url.
	  privacyPolicyUrl: 'https://docs.google.com/document/d/1nigpuXmgasl-PIqw84q36R4SeqTLTbaF-DmFEoNNmVM/edit?usp=sharing'
	};
	// The start method will wait until the DOM is loaded.
	ui.start('#firebaseui-auth-container', uiConfig);	
	
/**	
  // Sign into Firebase using popup auth & Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithPopup(provider).then(function(result) {
  var user = result.user;
  var userRef = firebase.firestore().collection("users").doc(user.uid);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					window.location.replace("client-dashboard");
			
			    } else {
					console.log("New user. Creat Reference");
					firebase.firestore().collection("users").doc(user.uid).set({
						name: user.displayName,
						email: user.email,
						profile_pic_url: user.photoURL,
						uid: user.uid,
						role: 'CLIENT'
					})
					.then(function() {
						console.log("Document successfully written!");
						window.location.replace("client-dashboard");
					})
					.catch(function(error) {
						console.error("Error writing document: ", error);
					});
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
			});
  // ...
}).catch(function(error) {
	  // Handle Errors here.
	  var errorMessage = error.message;
});
  // TODO 1: Sign in Firebase with credential from the Google user.
  
**/  
}



function proSignIn() {
	
	
	// Initialize the FirebaseUI Widget using Firebase.
	var ui = new firebaseui.auth.AuthUI(firebase.auth());	
	
	var uiConfig = {
	  callbacks: {
		signInSuccessWithAuthResult: function(authResult, redirectUrl) {
		  // User successfully signed in.
		  // Return type determines whether we continue the redirect automatically
		  // or whether we leave that to developer to handle.
		  
		  var user = authResult.user;
		  var userRef = firebase.firestore().collection("users").doc(user.uid);
					userRef.get().then(function(doc) {
						if (doc.exists) {
							window.location.replace("pro-requested-appt");
					
						} else {
							console.log("New user. Creat Reference");
							firebase.firestore().collection("users").doc(user.uid).set({
								'name': user.displayName,
								'email': user.email,
								'profile_pic_url': user.photoURL,
								'uid': user.uid,
								'role': 'PRO',
								'status': 'Inactive',
								'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
								'credit': 0.0,
								'rating_count': 1,
								'rating_sum': 5
							})
							.then(function() {
								console.log("Document successfully written!");
								window.location.replace("pro-profile-settings");
							})
							.catch(function(error) {
								console.error("Error writing document: ", error);
							});
						}
					}).catch(function(error) {
						console.log("Error getting user:", error);
					});
			
			
		  return false;
		},
		uiShown: function() {
		  // The widget is rendered.
		  // Hide the loader.
		  document.getElementById('loader').style.display = 'none';
		}
	  },
	  // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
	  signInFlow: 'popup',
	  signInSuccessUrl: '<url-to-redirect-to-on-success>',
	  signInOptions: [
		// Leave the lines as is for the providers you want to offer your users.
		firebase.auth.GoogleAuthProvider.PROVIDER_ID,
		firebase.auth.FacebookAuthProvider.PROVIDER_ID,
		firebase.auth.EmailAuthProvider.PROVIDER_ID
	  ],
	  // Terms of service url.
	  tosUrl: 'https://docs.google.com/document/d/1MZTxDNvX1Q9DvgdFj3gT_pN0G_bltFqOcz65Q5kW3Ys/edit?usp=sharing',
	  // Privacy policy url.
	  privacyPolicyUrl: 'https://docs.google.com/document/d/1nigpuXmgasl-PIqw84q36R4SeqTLTbaF-DmFEoNNmVM/edit?usp=sharing'
	};
	// The start method will wait until the DOM is loaded.
	ui.start('#firebaseui-auth-container', uiConfig);	
	
	
	/**
  // Sign into Firebase using popup auth & Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithPopup(provider).then(function(result) {
  var user = result.user;
  var userRef = firebase.firestore().collection("users").doc(user.uid);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					window.location.replace("pro-dashboard");
			
			    } else {
					console.log("New user. Creat Reference");
					firebase.firestore().collection("users").doc(user.uid).set({
						'name': user.displayName,
						'email': user.email,
						'profile_pic_url': user.photoURL,
						'uid': user.uid,
						'role': 'PRO',
						'status': 'Inactive',
						'credit': 0.0,
						'rating_count': 1,
						'rating_sum': 5
					})
					.then(function() {
						console.log("Document successfully written!");
						window.location.replace("pro-profile-settings");
					})
					.catch(function(error) {
						console.error("Error writing document: ", error);
					});
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
			});
  // ...
}).catch(function(error) {
	  // Handle Errors here.
	  var errorMessage = error.message;
});
  // TODO 1: Sign in Firebase with credential from the Google user.
  **/
}



// Signs-out of Friendly Chat.
function signOut() {
  // Sign out of Firebase.
	firebase.auth().signOut().then(function() {
  // Sign-out successful.
	window.location.replace("index");
	}).catch(function(error) {
	  // An error happened.
	});

}

function renderRating(rating){
			var ratingStars = '';
			switch(rating) {
			  case 1:
				ratingStars = 
				    '<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>';
				
				break;
			  case 2:
				ratingStars = 
				    '<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>';
				
				break;
				case 3:
				ratingStars = 
				    '<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>';
				
				break;
			  case 4:
				ratingStars = 
				    '<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star"></i>';
				
				break;
			   case 5:
				ratingStars = 
				    '<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>'
				+	'<i class="fas fa-star filled"></i>';
				
				break;
			   default:
				//No rating
				ratingStars = 
				    '<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>'
				+	'<i class="fas fa-star"></i>';
			}
			
			return ratingStars ;
			
		}
		

function acceptDeclineTransaction(id, stat){

	Swal.fire({
	  title: 'Change transaction status to: ' + stat,
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  showLoaderOnConfirm: true,
	  allowOutsideClick: () => !Swal.isLoading(),
	  preConfirm: () => {
		return firebase.firestore().collection("transactions").doc(id).update({
		'status': stat,
		'to_name': currentUserRef.name,
		'to_profile_pic': currentUserRef.profile_pic_url,
		'to_id': currentUserRef.uid
		})
		.then(function() {
			console.log("Document successfully written!");
			return true;
			
			
		})
		.catch(function(error) {
			console.error("Error writing document: ", error);
			return false;
		});
	  
	  
	  }
	  
	}).then((result) => {
	  if (result.value == true) {
		location.reload();
	  }
	});

}

function changeTransactionStatus(id, stat){

	Swal.fire({
	  title: 'Change transaction status to: ' + stat,
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  showLoaderOnConfirm: true,
	  allowOutsideClick: () => !Swal.isLoading(),
	  preConfirm: () => {
		return firebase.firestore().collection("transactions").doc(id).update({
		'status': stat
		})
		.then(function() {
			console.log("Document successfully written!");
			return true;
			
		})
		.catch(function(error) {
			console.error("Error writing document: ", error);
			return false;
		});
	  
	  
	  }
	  
	}).then((result) => {
	  if (result.value == true) {
		location.reload();
	  }
	});

}


function initTransaction(pro, service, duration, date, location){
	if(pro != null){
		
		if(currentUserRef == null){
			alert('Login to book appointment');
			return;
		}
		
		Swal.fire({
		  title: 'Request Appointment',
		  text: 'Request appointment with ' + pro.name,
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Proceed to confirmation',
		  showLoaderOnConfirm: true,
		  allowOutsideClick: () => !Swal.isLoading(),
		  preConfirm: () => {
			return firebase.firestore().collection("transactions").add({
			'status': 'Requested',
			'to_name': pro.name,
			'to_profile_pic': pro.profile_pic_url,
			'to_id': pro.uid,
			'from_name': currentUserRef.name,
			'from_id': currentUserRef.uid,
			'from_profile_pic': currentUserRef.profile_pic_url,
			'service': service,
			'duration': duration,
			'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
			'date': date,
			'location': location,
			'is_paid_in': false
			})
			.then(function() {
				console.log("Document successfully written!");
				return true;
			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
				return false;
			});
		  
		  
		  }
		  
		}).then((result) => {
		  if (result.value) {
			window.location.replace("client-requested-appt");
		  }
		});

	
	}else{

		if(currentUserRef == null){
			alert('Login to book appointment');
			return;
		}
		
		Swal.fire({
		  title: 'Broadcast Appointment',
		  text: 'Broadcast appointment to Locum Professionals ',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Create Broadcast',
		  showLoaderOnConfirm: true,
		  allowOutsideClick: () => !Swal.isLoading(),
		  preConfirm: () => {
			return firebase.firestore().collection("transactions").add({
			'status': 'Requested',
			'to_name': 'Broadcast',
			'to_profile_pic': 'Requested',
			'to_id': 'Requested',
			'from_name': currentUserRef.name,
			'from_id': currentUserRef.uid,
			'from_profile_pic': currentUserRef.profile_pic_url,
			'service': service,
			'duration': duration,
			'timestamp': firebase.firestore.FieldValue.serverTimestamp(),
			'date': date,
			'location': location,
			'is_paid_in': false
			})
			.then(function() {
				console.log("Document successfully written!");
				return true;
			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
				return false;
			});
		  
		  
		  }
		  
		}).then((result) => {
		  if (result.value) {
			window.location.replace("client-requested-appt");
		  }
		});

		
		
	}
	
	
}

function getSetUserProfile(page){
	firebase.auth().onAuthStateChanged(function(user) {
	  if (user) {
			// User is signed in.
			var userRef = firebase.firestore().collection("users").doc(user.uid);
			userRef.get().then(function(doc) {
				if (doc.exists) {
					//console.log("User data:", doc.data());
					currentUserRef = doc.data();
					
					if(doc.data().role == "CLIENT" && proPages.includes(page)){
						window.location.replace("client-requested-appt");
						return;
					}
					
					if(doc.data().role == "PRO" && clientPages.includes(page)){
						window.location.replace("pro-requested-appt");
						return;
					}
					
					if(doc.data().role == "PRO" && doc.data().status == "Active"){
						$('#user-status').hide();
					}
					
					document.getElementById('header-profile-name').innerHTML = doc.data().name ;
					document.getElementById('header-profile-email').innerHTML = doc.data().email ;
					document.getElementById('header-profile-pic-main').src = doc.data().profile_pic_url ;
					document.getElementById('header-profile-pic-sub').src = doc.data().profile_pic_url ;
					
					document.getElementById('user-name').innerHTML = doc.data().name ;
					document.getElementById('user-profile-pic').src = doc.data().profile_pic_url ;
					document.getElementById('user-email').innerHTML = '<i class="fas fa-envelope"></i>' + doc.data().email ;
					//document.getElementById('user-phone').innerHTML = '<i class="fas fa-phone"></i>' + doc.data().phone ;
					
				} else {
					// doc.data() will be undefined in this case
					console.log("No such user!");
				}
			}).catch(function(error) {
				console.log("Error getting user:", error);
			});
			
	  } else {
			// No user is signed in.
			window.location.replace("index");
	  }
	
	});
			
}


		

function loadClientTemp(page){
		getSetUserProfile(page);
	  $('#footer').load('templates/footer.html');
		   $('#header').load('templates/header.html', function() {
				if(isUserSignedIn){
					$('#client-login-control').hide();
					$('#pro-login-control').hide();
					
					$('#header-dashboard').attr("href", "client-requested-appt");
					$('#header-settings').attr("href", "client-profile-settings"); 
					
				}else{
					$('#header-profile').hide();
				}
			});
			
		$('#sidebar').load('templates/client-side-menu.html', function() {
				if(page == 'client-requested-appt'){
					$('#client-requested-appt').addClass('active');	
				}else if(page == 'client-accepted-appt'){
					$('#client-accepted-appt').addClass('active');	
				}else if(page == 'client-paid-appt'){
					$('#client-paid-appt').addClass('active');	
				}else if(page == 'client-completed-appt'){
					$('#client-completed-appt').addClass('active');	
				}else if(page == 'client-profile-settings'){
					$('#client-profile-settings').addClass('active');	
				}else if(page == 'client-invoices'){
					$('#client-invoices').addClass('active');	
				}else if(page == 'broadcasts'){
					$('#broadcasts').addClass('active');	
				}else{
					
				}
				
			});
}


function loadProTemp(page){
	getSetUserProfile(page);
	  $('#footer').load('templates/footer.html');
		   $('#header').load('templates/header.html', function() {
				if(isUserSignedIn){
					$('#client-login-control').hide();
					$('#pro-login-control').hide();
					$('#header-dashboard').attr("href", "pro-requested-appt");
					$('#header-settings').attr("href", "pro-profile-settings"); 
					
				}else{
					$('#header-profile').hide();
				}
			});
			
		$('#sidebar').load('templates/pro-side-menu.html', function() {
				if(page == 'pro-requested-appt'){
					$('#pro-requested-appt').addClass('active');	
				}else if(page == 'pro-accepted-appt'){
					$('#pro-accepted-appt').addClass('active');	
				}else if(page == 'pro-paid-appt'){
					$('#pro-paid-appt').addClass('active');	
				}else if(page == 'pro-profile-settings'){
					$('#pro-profile-settings').addClass('active');	
				}else if(page == 'pro-reviews'){
					$('#pro-reviews').addClass('active');	
				}else if(page == 'pro-invoices'){
					$('#pro-invoices').addClass('active');	
				}else{
					
				}
				
			});
}















// Initiate firebase auth.
function initFirebaseAuth() {
  // Listen to auth state changes.
  firebase.auth().onAuthStateChanged(authStateObserver);
}


// Returns true if a user is signed-in.
function isUserSignedIn() {
  return !!firebase.auth().currentUser;
}



function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      //paramName = paramName.toLowerCase();
      //if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}


// Triggers when the auth state change for instance when the user signs-in or signs-out.
function authStateObserver(user) {
  if (user) { // User is signed in!
    console.log("User is signed in!"); 
    // We save the Firebase Messaging Device token and enable notifications.
    //saveMessagingDeviceToken();

	
  } else { // User is signed out!
    console.log("User is signed out!");
	// Hide user's profile and sign-out button.
	
  }
}


// Checks that the Firebase SDK has been correctly setup and configured.
function checkSetup() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
  }
}

// Checks that Firebase has been imported.
checkSetup();


// initialize Firebase
initFirebaseAuth();



//searchDoctors();

// Remove the warning about timstamps change. 
/**
var firestore = firebase.firestore();
var settings = {timestampsInSnapshots: true};
firestore.settings(settings);
**/